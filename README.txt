Mail New User
=============

This module enables admin users to modify the welcome email subject and body at
the moment at which the email is about to be sent.

At the user creation page (admin/people/create), there is a checkbox "Notify
user of new account". When this is checked, the contents of the welcome email
normally at the Account settings page (admin/config/people/accounts) is 
now displayed and can be changed.  

Install this module in /sites/all/modules/mail_new_user.

